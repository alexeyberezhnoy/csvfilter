 # -*- coding: utf-8 -*-
import sys
import os
import re


class CSVFilterError(Exception):
    pass


class ComandLineError(Exception):
    pass


def parse_args(argv):
    if len(argv) != 3:
        raise ComandLineError(u'Неверное количество аргументов')
    return (argv[1], argv[2])

def main(file_path, result_path):
    try:
        csv = open(file_path, 'rU')
    except IOError:
        raise CSVFilterError('Невозможно открыть файл {}'.format(file_path))

    header = csv.readline()
    if header != 'street,city,zip,state,beds,baths,sq__ft,type,sale_date,price,latitude,longitude\n':
        raise CSVFilterError(u'Неверный заголовок таблиц')

    cities_db = {}
    zip_db = {}
    format_record = re.compile('^(\d+ [\w\d\s]+),([\w\s]+),(\d+),(\w+),(\d),(\d),(\d+),([\w\-]+),(\w+ \w+ \d+ \d+:\d+:\d+ \w+ \d+),(\d+),([+-]?\d+\.\d+),([+-]?\d+\.\d+)\n$')

    for row in csv:
        result = re.match(format_record, row)
        if not result:
            raise CSVFilterError(u'Невозможно распознать строку {0}'.format(row))
        record = result.groups()

        if record[1] in cities_db:
            cities_db[record[1]].append(record)
        else:
            cities_db[record[1]] = []

        if record[2] in zip_db:
            zip_db[record[2]].append(record)
        else:
            zip_db[record[2]] = []

    try:
        os.makedirs(result_path)
    except OSError:
        raise CSVFilterError('Невозможно создать папку {}'.format(result_path))
    index = open('{}/index.html'.format(result_path), 'w')
    index_data = ''

    index_data += '<h3>Grouped by city</h3>'
    for city in cities_db:
        index_data += "<p><a href='{}.html/'>{}<a><p>".format(city, city)
        file = open('{}/{}.html'.format(result_path, city), 'w')
        file.write(serialize_records(cities_db[city]))
        file.close()
    index.write(index_data)

    index_data += '<h3>Grouped by zip</h3>'
    for zip_code in zip_db:
        index_data += "<p><a href='{}.html/'>{}<a><p>".format(zip_code, zip_code)
        file = open('{}/{}.html'.format(result_path, zip_code), 'w')
        file.write(serialize_records(zip_db[zip_code]))
        file.close()
    index.write(index_data)

    index.close()

def serialize_records(records):
    data = '<table>'
    for record in records:
        data += '<tr>'
        for item in record: data += '<td>{}</td>'.format(item)
        data += '</tr>'
    data +='</table>'
    return data

if __name__ == "__main__":
    try:
        args = parse_args(sys.argv)
    except ComandLineError as e:
        print e
    else:
        try:
            main(*args)
        except CSVFilterError as e:
            print e
